<div id="sider-tools">
    <a class="qq" href="tencent://message/?uin=178304593&Site=www.ndesig.com&Menu=yes" target="_blank"><i class="iconfont icon-qq"></i></a>
    <a class="sshare" href="javascript:;" style="display:none;"><i class="iconfont icon-share"></i></a>
    <a href="javascript:;" class="sweixin"><i class="iconfont icon-weixin"></i></a>
    <a href="javascript:;" class="gotop J-gotop" style="visibility:hidden;"><i class="iconfont icon-arrup"></i></a>
</div>
{% require "./sider-tools.less" %}
{% script %}
    $(function(){
       //gotop
       var $top = $(".J-gotop");
       if($top.length) {
               $top.click(function(){
                   $('html, body').animate({scrollTop : 0},400);
                   return false;
               });
           };
       	$(window).scroll(function(){
               var $win = $(window);
               if($win.scrollTop() > $win.height()) {
                   $top.css({"visibility":"visible"});
               } else {
                   $top.css({"visibility":"hidden"});
               }
           }).trigger("scroll");
        //微信弹出
        $(".sweixin").click(function(){
        if(!$("#wx-pop").length) {
        var html = '';
        var imgWx = __uri('/static/home/static/css/i/wx.jpg');
        html += '<div class="mask" id="wx-pop"style="position: fixed; z-index: 9999; top: 0; left: 0; width: 100%; height: 100%; background: rgb(0,0,0); background: rgba(0,0,0,.6); overflow-y: auto; z-index: 100;"><div class="wx-box" style="padding:20px;border:1px solid #ddd;background: #fff;position: absolute;left:50%;top:50%;margin-left:-120px;margin-top:-120px;"><i></i></div></div>';
        $('body').append($(html));
        } else {
        $("#wx-pop").show();
        }

        });

        $('body').delegate('#wx-pop','click',function(){
        $(this).hide();
        })


    })
{% endscript %}
