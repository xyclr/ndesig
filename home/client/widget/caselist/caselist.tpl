<ul class="show_list clearfix">
    {% for post in postsCase -%}
    <li class="projectitem" tags="{{post.tags}}">
        <div class="show_list_box wow fadeInUp" data-wow-delay="0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;">
            <a href="/case/p/{{ post._id.toString() }}" class="show_list_box_t"><img src="{{post.thumb}}"></a>
            <div class="show_shade clearfix">
                <a href="/case/p/{{ post._id.toString() }}" class="show_shade_bg"></a>
                <a href="/case/p/{{ post._id.toString() }}" class="show_shade_txt">
                    <span>{{post.title}}</span>
                    <i>{{post.tags}}</i>
                </a>
                <a href="/case/p/{{ post._id.toString() }}" class="show_list_but"><i class="iconfont icon-arrrightside"></i></a>
            </div>
        </div>
    </li>
    {%- endfor %}
</ul>
{% require "./caselist.less" %}
