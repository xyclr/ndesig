{% extends 'home:page/layout_user.tpl' %}

{% block content %}

<div class="wrapper">

    {% include 'home:page/user/nav.tpl' %}

    <div class="page-wrapper">
        <div class="row white-bg page-heading">
            <div class="col-lg-12">
                <h3>文章</h3>
                <ol class="breadcrumb">
                    <li>
                        <strong>文件</strong>
                    </li>
                    <li class="active">
                        <strong>文件管理</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="wrapper wrapper-content mt20">
            <div class="row">
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="file-manager">

                                <div class="ajax-wrapper" >
                                    <button class="btn btn-primary btn-block">文件上传</button>
                                    <input id="fileupload" type="file" class="btn btn-primary btn-block" name="files[]" multiple="multiple">
                                </div>
                                <div id="progress" style="visibility: hidden" class="progress"><div class="progress-bar progress-bar-success"></div></div>
                                <!--<div id="files" class="files"></div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">

                            {% for file in files -%}
                            <div class="file-box">
                                <div class="file">
                                    <a href="{{ file.filePath }}" target="_blank">
                                        <span class="corner"></span>

                                        <div class="image">
                                            <img alt="image" class="img-responsive" src="{{ file.filePath }}">
                                        </div>
                                        <div class="file-name">
                                            {{ file.name }}
                                            <br>
                                            <small>{{ file.time }}</small>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            {%- endfor %}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{% require "home:static/js/jquery.ui.widget.js" %}
{% require "home:static/js/jquery.iframe-transport.js" %}
{% require "home:static/js/jquery.fileupload.js" %}
{% script %}
    /*jslint unparam: true */
    /*global window, $ */
    $(function() {

        var $progress = $("#progress");
        $('#fileupload').fileupload({
            url: '/user/file',
            dataType: 'json',
            //autoUpload: false,
            //acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 5000000, // 5 MB
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
            previewMaxWidth: 100,
            previewMaxHeight: 100,
            previewCrop: true
        }).on('fileuploadadd', function (e, data) {
            data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function (index, file) {
                var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                //if (!index) {
                //    node
                //        .append('<br>')
                //        .append(uploadButton.clone(true).data(data));
                //}
                node.appendTo(data.context);
            });
        }).on('fileuploadprocessalways', function (e, data) {
            var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
            if (file.preview) {
                node
                        .prepend('<br>')
                        .prepend(file.preview);
            }
            if (file.error) {
                node
                        .append('<br>')
                        .append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
            }
        }).on('fileuploadprogressall', function (e, data) {
            $progress.css({"visibility": "visible"});
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
            );
            $('#progress .progress-bar').text(progress + '%');
        }).on('fileuploaddone', function (e, data) {
            $progress.css({"visibility": "hidden"});
            $progress.find('.progress-bar').css({"width":"0"});
            $.each(data.result.files, function (index, file) {
                if (file.error) {
                    var error = $('<span class="text-danger"/>').text(file.error);
                    $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                }

                window.location.reload()
            });
        }).on('fileuploadfail', function (e, data) {
            $.each(data.files, function (index, file) {
                var error = $('<span class="text-danger"/>').text('File upload failed.');
                $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
            });
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

    });

{% endscript %}
<style>
    /* FILE MANAGER */
    .ajax-wrapper {
        position: relative;
    }
    #fileupload {
        position: absolute;
        left:0;
        top:0;
        opacity: 0;
    }
    #progress {
        margin-top:10px;
    }
    .file-box {
        float: left;
        width: 220px;
    }
    .file-manager h5 {
        text-transform: uppercase;
    }
    .file-manager {
        list-style: none outside none;
        margin: 0;
        padding: 0;
    }
    .folder-list li a {
        color: #666666;
        display: block;
        padding: 5px 0;
    }
    .folder-list li {
        border-bottom: 1px solid #e7eaec;
        display: block;
    }
    .folder-list li i {
        margin-right: 8px;
        color: #3d4d5d;
    }
    .category-list li a {
        color: #666666;
        display: block;
        padding: 5px 0;
    }
    .category-list li {
        display: block;
    }
    .category-list li i {
        margin-right: 8px;
        color: #3d4d5d;
    }
    .tag-list li {
        float: left;
    }
    .tag-list li a {
        font-size: 10px;
        background-color: #f3f3f4;
        padding: 5px 12px;
        color: inherit;
        border-radius: 2px;
        border: 1px solid #e7eaec;
        margin-right: 5px;
        margin-top: 5px;
        display: block;
    }
    .file {
        border: 1px solid #e7eaec;
        padding: 0;
        background-color: #ffffff;
        position: relative;
        margin-bottom: 20px;
        margin-right: 20px;
    }
    .file .icon,
    .file .image {
        height: 100px;
        overflow:hidden;
    }
    .file .icon {
        padding: 15px 10px;
        text-align: center;
    }
    .file .icon i {
        font-size: 70px;
        color: #dadada;
    }
    .file .file-name {
        padding: 10px;
        background-color: #f8f8f8;
        border-top: 1px solid #e7eaec;
        text-overflow:ellipsis;
        white-space:nowrap;
        word-break: break-all;
        overflow: hidden;
    }
    .file-name small {
        color: #676a6c;
    }
    .corner {
        position: absolute;
        display: inline-block;
        width: 0;
        height: 0;
        line-height: 0;
        border: 0.6em solid transparent;
        border-right: 0.6em solid #f1f1f1;
        border-bottom: 0.6em solid #f1f1f1;
        right: 0em;
        bottom: 0em;
    }
</style>
{% endblock %}
