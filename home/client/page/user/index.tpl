{% extends 'home:page/layout_user.tpl' %}

{% block content %}

<div class="wrapper">
    {% require "home:static/css/user.less" %}
    {% require "home:static/css/summernote.css" %}

    {% include 'home:page/user/nav.tpl' %}

    <div class="page-wrapper">
        <div class="row white-bg page-heading">
            <div class="col-lg-12">
                <h3>发布文章</h3>
                <ol class="breadcrumb">
                    <li>
                        <strong>发布</strong>
                    </li>
                    <li class="active">
                        <strong>发布文章</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row mt20">
            <div class="col-lg-12">
                <div class="ibox-title">
                    <h5>发布文章</h5>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" id="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="title">标题</label>
                            <div class="col-sm-10"><input type="text" class="form-control" id="title" name="title" placeholder="文章标题"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">标签</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4"><input type="text" placeholder=".标签" class="form-control" name="tag"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">推荐位置</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4"><input type="text" placeholder="1（首页banner）" class="form-control" name="posi"></div>
                                </div>
                            </div>
                        </div>

                        <!--extra start-->

                        <div class="form-group">
                            <label class="col-sm-2 control-label">机执行机构名称构</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4"><input type="text" placeholder=".执行机构名称" class="form-control" name="org1"></div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">捐款机构名称</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4"><input type="text" placeholder=".捐款机构名称" class="form-control" name="org2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">项目持续时间</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4"><input type="text" placeholder=".时间（2015/7/16 - 2015/8/16）" class="form-control" name="casetime"></div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label">项目目标</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4"><input type="text" placeholder=".目标" class="form-control" name="target"></div>
                                </div>
                            </div>
                        </div>

                        <!--extra end-->

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="thumb">缩略图</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="input-group" data-type="file">
                                            <input type="text" class="form-control cur" name="thumb" placeholder="请输入或者选择图片地址">
                                            <span class="input-group-btn"> <a class="btn btn-default btn-upload" data-toggle="modal" data-target="#fileUpload">选择</a> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">正文</label>
                            <div class="col-sm-10">
                                <!-- <textarea name="post" rows="20" cols="100" class="form-control" placeholder="请输入..."></textarea>-->
                                <textarea name="post"  class="summernote"><p>请输入...</p></textarea>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2"> <button type="submit" class="btn btn-primary">发表</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{% require "home:static/js/summernote.js" %}
{% script %}
    var User = require("home:client/static/js/page/user.js");

    $(function(){

        User.init();

    })
{% endscript %}
{% endblock %}
