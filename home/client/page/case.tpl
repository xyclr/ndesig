{% extends 'home:page/layout.tpl' %}

{% block content %}
    {% require "home:static/css/inner.less" %}
    {% widget "home:widget/header/header.tpl"%}


<div class="inner-content" style="margin-top: 80px;">
    <div class="mt">
        <h3>案例</h3>
        <p>SHOWCASE</p>

        <div class="category">
            <a href="#" class="active">全部</a>
            <a href="#">WEB</a>
            <a href="#">移动端</a>
        </div>
    </div>
    <div class="mc">
        {% widget "home:widget/caselist/caselist.tpl"%}
    </div>
</div>
    {% widget "home:widget/home_footer/home_footer.tpl"%}
    {% widget "home:widget/sider_tools/sider_tools.tpl"%}
{% script %}
    $(function(){
        var ret = '<a href="#" class="active">全部</a>';
        var $cat = $(".category");
        $.ajax({
            type: 'GET',
            url: "/tag" ,
            dataType: 'json',
            success: function(data){
                    $cat.html(function(){
                    $.each(data,function(i,v){
                        ret += '<a href="#">' + v.title + '</a>';
                    });
                    return ret;
                })
            }
        });

        $cat.delegate('a','click',function(){
            var that = $(this);
            that.addClass('active').siblings().removeClass('active');
            if(that.text() === "全部") {$(".show_list li").show();return false};
            $(".show_list li").each(function(item){
                var me = $(this);
                if(me.attr('tags').indexOf(that.text()) !== -1) {
                    me.show();
                } else {
                    me.hide();
                }
            });
            return false;
        })
    })
{% endscript %}
{% endblock %}
