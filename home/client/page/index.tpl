{% extends 'home:page/layout.tpl' %}

{% block content %}
    {% require "home:static/css/index.less" %}
    {% widget "home:widget/header/header.tpl"%}
    {% widget "home:widget/home_banner/home_banner.tpl"%}

<div class="content">

    <!-- service -->
    {% widget "home:widget/home_service/home_service.tpl"%}

    <!-- case -->
    <div class="module showcase-bg">
        <div class="bgmask"></div>
        <div class="showcase module-box">
            <div class="show_top clearfix module-tit">
                <div class="fadeInLeft" data-wow-delay="0.1s"
                     style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">
                    <h2>案例</h2>

                    <p>SHOWCASE</p>
                </div>
            </div>
            <div class="module-bot module-cnt">
                {% widget "home:widget/caselist/caselist.tpl"%}
                <div class="clearfix"></div>
                <a href="/case" class="module-more">VIEWS MORE</a>
            </div>
        </div>
    </div>

    <!--about-->
    <div class="module" >
        <div class="bgmask"></div>
        <div class="about module-box">
            <div class="clearfix module-tit">
                <div class="fadeInLeft">
                    <h2>新闻</h2>
                    <p >NEWS</p>
                </div>
            </div>
            <div class="module-bot">
                {% widget "home:widget/newlist/newlist.tpl"%}
            </div>
        </div>
    </div>

    <!--news-->
    <div class="module news_bg" >


        <div class="bgmask"></div>
        <div class="about module-box">
            <div class="clearfix module-tit">
                <div class="fadeInLeft">
                    <h2>关于</h2>
                    <p >ABOUT</p>
                </div>
            </div>
            <div class="module-bot">
                <div class="about_bot">
                    专为热爱界面设计的你而倾力打造。这里拥有最无敌的创意、最精美的视觉、最具国际化的意识和前瞻性思维！也许在不知不觉中，它将成为你设计路上的一位好伙伴，在你最需要灵感的时候给予你帮助，鼓励你不断向前!
                    当然，我们也需要最热心的你，为我们平台注入源源不断的新鲜血液。
                </div>
                <img class="fr" style="width: 550px;" src="http://www.ndesig.com/static/home/static/upload/mDvIbSL9cW4AUiKVNTJJOPFt.jpg">
            </div>
        </div>
    </div>

    <!--flow-->
    <div class="module wflow_bg">
        <div class="bgmask"></div>
        <div class="wflow module-box">
            <div class="clearfix module-tit">
                <div class="fadeInLeft">
                    <h2>工作流</h2>
                    <p >WORK FLOW</p>
                </div>
            </div>
            <div class="module-bot">
                <ul class="wflow-list">
                    <li>
                        <div class="wflow-step">
                            <i class="i-img i-img-flask"></i>
                            <h4 class="wflow-title"> <i class="iconfont icon-numberone"></i><span>需求分析</span></h4>
                            <hr class="separator">
                            <p class="wflow-step-text">为我们的客户的业务收集和分析所有数据和需求。</p>
                        </div>
                    </li>
                    <li>
                        <div class="wflow-step">
                            <i class="i-img i-img-pc"></i>
                            <h4 class="wflow-title"> <i class="iconfont icon-numbertwo"></i><span>设计&开发</span></h4>
                            <hr class="separator">
                            <p class="wflow-step-text">规划、线框图、原型、可视化、编码。</p>
                        </div>
                    </li>
                    <li>
                        <div class="wflow-step">
                            <i class="i-img i-img-arr"></i>
                            <h4 class="wflow-title">  <i class="iconfont icon-numberthree"></i><span>测试</span></h4>
                            <hr class="separator">
                            <p class="wflow-step-text">测试、调试最终的项目。</p>
                        </div>
                    </li>
                    <li>
                        <div class="wflow-step">
                            <i class="i-img i-img-tar"></i>
                            <h4 class="wflow-title"> <i class="iconfont icon-numberfour"></i><span>发布</span></h4>
                            <hr class="separator">
                            <p class="wflow-step-text">将用户的产品发布到正式使用。</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

    {% widget "home:widget/home_footer/home_footer.tpl"%}
    {% widget "home:widget/sider_tools/sider_tools.tpl"%}
{% endblock %}
