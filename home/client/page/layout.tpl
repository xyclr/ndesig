<!doctype html>
{% html lang="en" framework="home:static/js/mod.js" %}
    {% head %}
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="/static/favicon.ico">
        <title>{{ title }}</title>
        {% require "home:static/css/base.less" %}
        {% require "home:static/js/jquery.js" %}
        {% require "home:static/js/common.js" %}
    {% endhead %}

    {% body %}
        {% block content %}
        {% endblock %}

    {% endbody %}

{% endhtml %}
