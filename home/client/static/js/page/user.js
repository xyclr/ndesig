var User = {
    init : function(){

        var $pageWrapper = $(".page-wrapper");
        var $summernote = $(".summernote");
        var $cat = $("#cat");
        var $tag = $("#tag");
        var curCat = $cat.attr("curIndex");
        var curTag = $tag.attr("tags");

        this.event();

        $pageWrapper.css({"min-height": $(window).height()});

        $summernote.length && $summernote.summernote({
            codemirror: {
                theme: 'monokai'
            }
        });

        //初始化分类
        if($cat.length) {

            $.ajax({
                type: 'GET',
                url: "/tag" ,
                dataType: 'json',
                success: function(data){
                    var ret = [];
                    if(data.length === 0) return;
                    $.each(data,function(i,v){
                        ret.push('<div class="checkbox f-ib mr10"> <label> <input type="checkbox"  ' + (curTag.toString().indexOf(v.title) !== -1  && 'checked="true"') + '>' + v.title +  '</label> </div>');
                    });
                    $tag.html(ret.join(""));
                }
            });
        };

        //初始化分类
        if($tag.length) {

            $.ajax({
                type: 'GET',
                url: "/cat" ,
                dataType: 'json',
                success: function(data){
                    var ret = [];
                    if(data.length === 0) return;
                    $.each(data,function(i,v){
                        ret.push('<option value="' + v.title +  '" name="' + v.name +  '" ' + (curCat === v.title && 'selected="selected"') + '>' + v.title +  '</option>');
                    });
                    $cat.html(ret.join(""));
                    $cat.val(curCat);
                }
            });
        }
    },

    event : function(){
        var $addCat = $(".J-add-cat");
        var $saveCat = $(".J-save-cat");
        var $addTag = $(".J-add-tag");
        var $saveTag = $(".J-save-tag");
        var $catNew = $("input[name=catNew]");
        var $tagNew = $("input[name=tagNew]");
        var $cat = $("#cat");
        var $tag = $("#tag");
        var $tagInput = $("input[name=tag]");

        if($addCat.length) {

            $addCat.click(function(){
                var me = $(this);
                me.prev().removeClass("collapse");
                me.addClass("collapse");
                me.next().removeClass("collapse");
            });

            $saveCat.click(function(){
                if($catNew.val() === "") return;
                var me = $(this);

                $.ajax({
                    type: 'POST',
                    url: "/cat" ,
                    dataType: 'json',
                    data: {
                        "title":$.trim($catNew.val())
                    },
                    success: function(data){
                        $cat.append('<option value="' + data.title +  '" name="' + data.name +  '">' + data.title +  '</option>')
                        me.prev().removeClass("collapse");
                        me.addClass("collapse");
                        me.prev().prev().addClass("collapse").val("");
                    }
                });
            })
        };

        if($addTag.length) {

            $addTag.click(function(){
                var me = $(this);
                me.prev().removeClass("collapse");
                me.addClass("collapse");
                me.next().removeClass("collapse");
            });

            $saveTag.click(function(){
                if($tagNew.val() === "") return;
                var me = $(this);

                $.ajax({
                    type: 'POST',
                    url: "/tag" ,
                    dataType: 'json',
                    data: {
                        "title":$.trim($tagNew.val())
                    },
                    success: function(data){
                        $tag.append('<div class="checkbox f-ib mr10"> <label> <input type="checkbox">' + data.title +  '</label> </div>');
                        me.prev().removeClass("collapse");
                        me.addClass("collapse");
                        me.prev().prev().addClass("collapse").val("");
                    }
                });
            })
        };

        $tag.delegate('input[type=checkbox]','click',function(){
            var tagsStr = '';

            $tag.find('input[type=checkbox]').each(function(){
                var me = $(this);
                if(me.prop("checked") == true) {
                    tagsStr += "/" + $.trim(me.parent().text());
                }
            });
            $tagInput.val(tagsStr.substr(1));
        });
    }
};

module.exports = User;