

$(function(){
    //header scroll event
    var $header = $("#header");
    $(window).scroll(function(){
        $('body').scrollTop() === 0 ? $header.removeClass('mini') : $header.addClass('mini');
    });

    //nav 定位
    var posi = window.location.href.split("/")[3];
    var $nav = $("#nav>li");
    if(posi === "") {
        $nav.eq(0).addClass("active");
    } else {
        $nav.each(function(){
            var me = $(this);

            if(me.attr("name") === posi) me.addClass("active");
        })
    };
})