var conf = require('./conf');

module.exports = {
    checkLogin : function (req, res, next) {
        if (!req.session.user) {
            req.flash('error', '未登录!');
            return res.redirect('/login');  //一定要return 不然报错 “Can't set headers after they are sent.”
        }
        next();
    },

    checkNotLogin : function (req, res, next) {
        if (req.session.user) {
            req.flash('error', '已登录!');
            return res.redirect('back');
        }
        next();
    },

    isRegister : conf.isRegister

};