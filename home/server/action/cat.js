var util = require("../lib/util.js");
var Cat = require("../model/cat.js");
var crypto = require('crypto');

module.exports.get = function (req, res, next) {

    Cat.get(function(err,cats){
        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }

        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(cats));

    });

};

module.exports.post = function (req, res) {

    Cat.get(function (err, cats) {

        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }

        var len = cats.length + 1;
        var cat = new Cat("cat" + len,len, req.body.title);
        cat.save(function (err) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            req.flash('success', '发布成功!');
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify({
                name : "cat" + len,
                id : len,
                title : req.body.title
            }));
        });
    });
};