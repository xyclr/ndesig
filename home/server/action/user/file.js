var util = require("../../lib/util.js"),
    multiparty = require('multiparty'),
    http = require('http'),
    fs = require('fs'),
    moment = require('moment'),
    path = require('path');

var stats = [];

module.exports = function (req, res, next) {
    util.checkLogin(req, res, next);
};


module.exports.get = function (req, res, next) {
    var uploadPath= 'static/home/static/upload/';
    if (!fs.existsSync(uploadPath)) {
        fs.mkdirSync(uploadPath);
    }
    fs.readdir('static/home/static/upload/', function (err, files) {

        var ret = [];
        if (err) {
            console.error(err);
            return;
        } else {
            //files.forEach(function (file) {
            //    var filePath = path.normalize(__dirname + '/fsDir/' + file);
            //    fs.stat(filePath, function (err, stat) {
            //        console.log(filePath + ' is: ' + 'file');
            //    });
            //});


            for (var i = 0; i < files.length; i++) {
                //使用闭包无法保证读取文件的顺序与数组中保存的致
                (function () {
                    var filePath = path.normalize('/static/home/static/upload/' + files[i]);
                    ret.push({
                        filePath: filePath,
                        name: files[i]
                    });

                })();
            }

            console.info(files.length);
            console.log(ret);
            res.render('home/page/user/file.tpl', {
                title: "文件管理 - N设工作室",
                user: req.session.user,
                files: ret
            });
        }
    });

};



module.exports.post = function (req, res, next) {
    var form = new multiparty.Form({
        uploadDir: 'static/home/static/upload/',
        maxFieldsSize: 2 * 1024 * 1024
    });

    //form.on('error', function(err) {
    //    console.log('Error parsing form: ' + err.stack);
    //});
    //
    //// Parts are emitted when parsing the form
    //form.on('part', function(part) {
    //    console.info('start upload');
    //    console.log(part)
    //    // You *must* act on the part by reading it
    //    // NOTE: if you want to ignore it, just call "part.resume()"
    //
    //    if (!part.filename) {
    //        // filename is not defined when this is a field and not a file
    //        console.log('got field named ' + part.name);
    //        // ignore field's content
    //        part.resume();
    //    }
    //
    //    if (part.filename) {
    //        // filename is defined when this is a file
    //        count++;
    //        console.log('got file named ' + part.name);
    //        // ignore file's content here
    //        part.resume();
    //    }
    //
    //    part.on('error', function(err) {
    //        // decide what to do
    //    });
    //});
    //
    //// Close emitted after form parsed
    //form.on('close', function() {
    //    console.log('Upload completed!');
    //});


    form.parse(req, function(err, fields, files) {
        console.log(files);
        res.writeHead(200, {'content-type': 'text/plain'});
        res.write(JSON.stringify({
            code: '0',
            files: files['files[]']
        }));
        res.end();
    });

    return;


};