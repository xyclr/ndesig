var util = require("../../lib/util.js");
var Post = require('../../model/post');

module.exports = function (req, res, next) {
    util.checkLogin(req, res, next);
};

module.exports.get = function (req, res, next) {

    Post.getArchive(function(err,posts){

        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }
        res.render('home/page/user/post.tpl', {
            title : "用户中心 - N设工作室",
            user: req.session.user,
            posts : [{
                post : "post",
                title : posts
            }]

        });
    });
}

module.exports.post = function (req, res, next) {

    var post = new Post( req.body.title, req.body.tag, req.body.post,req.body.thumb,req.body.posi,req.body.cat);
    post.save(function (err) {
        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }
        req.flash('success', '发布成功!');
        res.redirect('/user/archive');
    });
};