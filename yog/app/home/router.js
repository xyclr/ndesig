var util = require("./lib/util.js");
var Post = require('./model/post');

module.exports = function(router){
    // you can add app common logic here
    // router.use(function(req, res, next){
    // });

    // also you can add custom action
    // require /spa/some/hefangshi
    // router.get('/some/:user', router.action('api'));

    // or write action directly
    // router.get('/some/:user', function(res, req){});

    // a restful api example
    router.get('/logout', function(req,res){
        console.info(req.session);
        req.session.user = null;
        req.flash('success', '登出成功!');
        console.info("登出成功");
        res.redirect('/');
    });

    router.get('/case/p/:_id', function(req, res){
        Post.getOne(req.params._id, function (err, post) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            res.render('home/page/caseDetail.tpl', {
                title: post.title,
                post: post
            });
        });
    });
    router.get('/new/p/:_id', function(req, res){
        Post.getOne(req.params._id, function (err, post) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            res.render('home/page/newDetail.tpl', {
                title: post.title,
                post: post
            });
        });
    });


    router.get('/user/p/:_id', function(req, res){
        Post.getOne(req.params._id, function (err, post) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            res.render('home/page/user/article.tpl', {
                title: post.title,
                post: post,
                user: req.session.user,
                success: req.flash('success').toString(),
                error: req.flash('error').toString()
            });
        });
    });

    router.get('/user/p/edit/:_id', function(req, res){
        Post.edit(req.params._id, function (err, post) {
            if (err) {
                req.flash('error', err);
                return res.redirect('back');
            }
            res.render('home/page/user/edit.tpl', {
                title: post.title,
                post: post,
                user: req.session.user,
                success: req.flash('success').toString(),
                error: req.flash('error').toString()
            });
        });
    });

    router.post('/user/p/edit/:_id', function(req, res){
        Post.update(req.params._id,req.body.title, req.body.tag, req.body.post,req.body.thumb,req.body.posi,req.body.cat,req.body.abstract, function (err) {
            var url = '/user/p/' + req.params._id;
            if (err) {
                req.flash('error', err);
                return res.redirect(url);//出错！返回文章页
            }
            req.flash('success', '修改成功!');
            res.redirect(url);//成功！返回文章页
        });
    });

    router.get('/user/p/remove/:_id', function (req, res) {
        Post.remove(req.params._id, function (err) {
            if (err) {
                req.flash('error', err);
                return res.redirect('back');
            }
            req.flash('success', '删除成功!');
            res.redirect('/user');
        });
    });
};