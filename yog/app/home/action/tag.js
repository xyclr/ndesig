var util = require("../lib/util.js");
var Tag = require("../model/tag.js");
var crypto = require('crypto');

module.exports.get = function (req, res, next) {

    Tag.get(function(err,tags){
        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }

        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(tags));

    });

};

module.exports.post = function (req, res) {

    Tag.get(function (err, tags) {

        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }

        var len = tags.length + 1;
        var tag = new Tag("tag" + len,len, req.body.title);
        tag.save(function (err) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/');
            }
            req.flash('success', '发布成功!');
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify({
                name : "tag" + len,
                id : len,
                title : req.body.title
            }));
        });
    });
};