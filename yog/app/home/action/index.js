//var index = require('../model/index.js');
var util = require('../lib/util.js');
var Post = require('../model/post');

module.exports.get = function (req, res, next) {

    var postsBanner,postsCase,postsNew;

    Post.getPostsByParams({ "cat": "首页banner"},{"title":1,"thumb":1,"tags":"1","time":1},function(err,data1){

        postsBanner = data1;

        Post.getPostsByParams({ "cat": "案例"},{"title":1,"thumb":1,"tags":"1","time":1},function(err,data2){

            postsCase = data2;

            Post.getPostsByParams({ "cat": "新闻","posi":1},{"title":1,"thumb":1,"tags":"1","time":1,"abstract":1},function(err,data3){
                postsNew = data3;

                res.render('home/page/index.tpl', {
                    title : "首页 - N设工作室",
                    postsBanner : postsBanner,
                    postsCase : postsCase,
                    postsNew : postsNew
                });

            })

        });

    });


}