//var index = require('../model/index.js');
var util = require('../lib/util.js');
var Post = require('../model/post');


module.exports.get = function (req, res, next) {

    Post.getArchive(function(err,posts){

        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }
        console.info("posts");
        console.info(posts);
        console.info(JSON.stringify(posts));
        res.render('home/page/index.tpl', {
            title : "首页 - N设工作室",
            posts : [{
                post : "post",
                title : JSON.stringify(posts)
            }]

        });
    });
}