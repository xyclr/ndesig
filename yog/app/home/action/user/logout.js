module.exports.get = function (req, res, next) {
    req.session.user = null;
    req.flash('success', '登出成功!');
    res.redirect('/');
};