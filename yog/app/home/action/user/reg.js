var util = require("../../lib/util.js");
var User = require("../../model/user.js");
var crypto = require('crypto');

module.exports.get = function (req, res, next) {
    console.info("注册");
    res.render('home/page/user/reg.tpl', {
        title : "注册"
    });
};

module.exports.post = function (req, res) {
    var name = req.body.name,
        password = req.body.password,
        password_re = req.body['password-repeat'];
    if (password_re != password) {
        req.flash('error', '<b>两次输入的密码不一致!</b>');
        return res.redirect('/reg');
    }
    var md5 = crypto.createHash('md5'),
        password = md5.update(req.body.password).digest('hex');
    var newUser = new User({
        name: name,
        password: password,
        email: req.body.email
    });
    User.get(newUser.name, function (err, user) {
        if (user) {
            req.flash('error', '用户已存在!');
            return res.redirect('/reg');
        }
        newUser.save(function (err, user) {
            if (err) {
                req.flash('error', err);
                return res.redirect('/reg');
            }
            req.session.user = user;
            req.flash('success', '注册成功!');
            res.redirect('/');
        });
    });
};