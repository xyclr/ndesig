var util = require("../../lib/util.js");
var Post = require('../../model/post');

module.exports = function (req, res, next) {
    util.checkLogin(req, res, next);
};

module.exports.get = function (req, res, next) {

    Post.getArchive(function (err, posts) {

        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }
        console.info("posts");
        console.info(posts);
        console.info(JSON.stringify(posts));
        res.render('home/page/user/archive.tpl', {
            title: "归档 - N设工作室",
            user: req.session.user,
            posts: posts
        });
    });
};



module.exports.post = function (req, res, next) {

    var post = new Post(req.body.title, req.body.tag, req.body.post, req.body.thumb, [
        req.body.org1,
        req.body.org2,
        req.body.casetime,
        req.body.target
    ], req.body.posi);
    post.save(function (err) {
        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }
        req.flash('success', '发布成功!');
        res.redirect('/archive');
    });
};