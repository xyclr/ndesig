var util = require("../../lib/util.js");
var Post = require('../../model/post');

module.exports = function (req, res, next) {
    util.checkLogin(req, res, next);
};

module.exports.get = function (req, res, next) {
    Post.getOne(req.params._id, function (err, post) {
        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }
        res.render('article', {
            title: post.title,
            post: post,
            user: req.session.user,
            success: req.flash('success').toString(),
            error: req.flash('error').toString()
        });
    });
};

