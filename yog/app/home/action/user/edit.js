var util = require("../../lib/util.js");
var Post = require('../../model/post');

module.exports = function (req, res, next) {
    util.checkLogin(req, res, next);
};

module.exports.get = function (req, res, next) {

    Post.edit(req.params._id, function (err, post) {
        if (err) {
            req.flash('error', err);
            return res.redirect('back');
        }
        res.render('edit', {
            title : "文章编辑 - 发布 - N设工作室",
            post: post,
            user: req.session.user,
            success: req.flash('success').toString(),
            error: req.flash('error').toString(),
            settings : settings
        });
    });
};

module.exports.post = function (req, res, next) {

    var post = new Post(req.body.title, req.body.tag, req.body.post,req.body.thumb,req.body.posi,req.body.cat);
    console.info(req.body.cat);
    post.save(function (err) {
        if (err) {
            req.flash('error', err);
            return res.redirect('/');
        }
        req.flash('success', '发布成功!');
        res.redirect('/user/archive');
    });
};