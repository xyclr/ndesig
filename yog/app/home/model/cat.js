var mongoose = require('./db');
var catSchema = new mongoose.Schema({
    name: String,
    id: String,
    title: String
}, {
    collection: 'cat'
});

var catModel = mongoose.model('Cat', catSchema);

function Cat(name,id,title) {
    this.name = name;
    this.id = id;
    this.title = title;

};

module.exports = Cat;

//存储分类
Cat.prototype.save = function(callback) {
    //要存入数据库的用户文档
    var cat = {
        name: this.name,
        id: this.id,
        title: this.title
    };

    var newCat = new catModel(cat);

    newCat.save(function (err, cat) {
        if (err) {
            return callback(err);
        }
        callback(null, cat);
    });
};

//返回所有文章存档信息
Cat.get = function (callback) {
    catModel.find({},{
        "name": 1,
        "id": 1,
        "title": 1
    }, {},function (err, docs) {
        if (err) {
            return callback(err);
        }
        callback(null,docs);
    });
};