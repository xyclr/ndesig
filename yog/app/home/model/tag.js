var mongoose = require('./db');
var tagSchema = new mongoose.Schema({
    name: String,
    id: String,
    title: String
}, {
    collection: 'tag'
});

var tagModel = mongoose.model('Tag', tagSchema);

function Tag(name,id,title) {
    this.name = name;
    this.id = id;
    this.title = title;

};

module.exports = Tag;

//存储分类
Tag.prototype.save = function(callback) {
    //要存入数据库的用户文档
    var tag = {
        name: this.name,
        id: this.id,
        title: this.title
    };

    var newTag = new tagModel(tag);

    newTag.save(function (err, tag) {
        if (err) {
            return callback(err);
        }
        callback(null, tag);
    });
};

//返回所有文章存档信息
Tag.get = function (callback) {
    tagModel.find({},{
        "name": 1,
        "id": 1,
        "title": 1
    }, {},function (err, docs) {
        if (err) {
            return callback(err);
        }
        callback(null,docs);
    });
};