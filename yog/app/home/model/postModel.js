var mongoose = require('./db');

var postSchema = new mongoose.Schema({
    name: String,
    title: String,
    tags: String,
    post: String,
    time: {},
    comment : Array,
    thumb : String,
    caseinfo : Array,
    posi : String,
    cat : String,
    abstract : String,
    extra :{}
}, {
    collection: 'posts'
});

module.exports = mongoose.model('Post', postSchema);
