/**
 * Created by Alex on 16/1/31.
 */
var settings = require('../../settings');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);


module.exports.userPluginsA = function(app, conf){
    app.use(session({
        secret: settings.cookieSecret,
        key: settings.db,//cookie name
        cookie: {maxAge: 1000 * 60 * 60 * 24 * 30},//30 days
        store: new MongoStore({
            db: settings.db,
            host: settings.host,
            port: settings.port
        })
    }));
}