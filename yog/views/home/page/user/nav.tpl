<div class="navbar-static-side">
    <div class="sidebar-collapse">
        <ul class="nav">
            <li class="nav-header">
                <div class="profile-element">
                    <span> <img alt="image" class="img-circle" src="/static/home/static/css/i/profile_small_d41e8a8.jpg"> </span>
                    <strong>{{user.name}}</strong>
                </div>
            </li>
            <li> <a href="#"><i class="fa fa-plus-square"></i> <span class="nav-label">发布</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level" aria-expanded="false">
                    <li><a href="/user/post">发布文章</a></li>
                    <li><a href="/user/archive">文章管理</a></li>
                </ul>
            </li>
            <li> <a href="/user/file"><i class="fa fa-file"></i> <span class="nav-label">文件管理</span></a></li>
        </ul>
    </div>
</div>