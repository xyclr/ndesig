{% extends 'home:page/layout_user.tpl' %}

{% block content %}

<div class="wrapper">

    {% include 'home:page/user/nav.tpl' %}

    <div class="page-wrapper">
        <div class="row white-bg page-heading">
            <div class="col-lg-12">
                <h3>文章管理</h3>
                <ol class="breadcrumb">
                    <li>
                        <strong>发布</strong>
                    </li>
                    <li class="active">
                        <strong>文章管理</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row mt20">
            <div class="col-lg-12">
                <div class="ibox-title">
                    <h5>文章管理</h5>
                </div>
                <div class="ibox-content">
                    <table class="table table-hover J-archvie-list">
                        <thead>
                        <tr>
                            <th>标题</th>
                            <th>分类</th>
                            <th>推荐位置</th>
                            <th>发布者</th>
                            <th>时间</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for post in posts -%}
                            <tr>
                                <th><a href='/user/p/{{ post._id.toString() }}' target="_blank">{{ post.title }}</a></th>
                                <td>{{ post.cat }}</td>
                                <td>{{ post.posi }}</td>
                                <td>{{user.name}}</td>
                                <td>{{ post.time.day }}</td>
                            </tr>
                        {%- endfor %}
                        </tbody>
                        <tfoot>
                            <tr>
                                <tr>
                                    <td colspan="5">
                                        <div class="pager J-pager">

                                        </div>
                                    </td>
                            </tr>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{% require "home:static/js/summernote.js" %}
{% require "home:widget/pagination/pagination.less" %}
{% script %}
    var User = require("home:client/static/js/page/user.js");
    //var Pager = require("home:widget/pagination/pagination.js");

    $(function(){

        var len = $(".J-archvie-list tbody tr").length;

        User.init();

        <!--$(".J-archvie-list tbody tr").eq(0).show();-->
        <!--Pager($(".J-pager"),{-->
                <!--pageCount:Math.ceil(len/2),-->
                <!--current:1,-->
                <!--backFn:function(p){-->
                    <!--$(".J-archvie-list tbody tr").hide().eq(p-1).show();-->
            <!--}-->
        <!--});-->

    })
{% endscript %}
{% require "home:page/user/archive.tpl" %}{% endblock %}
