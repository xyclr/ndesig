{% extends 'home:page/layout.tpl' %}

{% block content %}
    {% require "home:static/css/inner.less" %}
    {% widget "home:widget/header/header.tpl"%}
<div class="banner">
    <div style=" background-color: #39383E; background-repeat: no-repeat; background-position: center; background-size: cover;width: 100%; height: 380px;background-image: url(http://www.ndesig.com/static/home/static/upload/jRL9Fcn4CQ3kt0fHqWHcFO5L.jpg);">

    </div>
</div>

<div class="inner-content">
    <div class="mt">
        <h3>联系我们</h3>
        <p>CONTACT US</p>
    </div>
    <div class="mc">
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <table border="0" width="1180">
            <tbody>
            <tr class="firstRow">
                <td style="width: 48%; word-break: break-all;" valign="top">
                    <img style="float:right;" src="http://www.ndesig.com/static/home/static/upload/HJoueKAWi-uiDvrDvE4ovKJv.png"></td>
                <td style="width:4%">&nbsp;</td>
                <td style="width: 48%; word-break: break-all;" valign="top">
                    <p><strong><span style="font-size: 16px;">成都N设工作室</span></strong></p>
                    <p><br></p>
                    <br>
                    <p class="ellipsis">Address：中国-成都市-锦江区</p>
                    <br>
                    <p class="ellipsis">Zipcode：610000</p>
                    <br>
                    <p class="ellipsis">Mobile：18628082771</p>
                    <br>
                    <p class="ellipsis">Email：18628082771@qq.com</p>
                    <br>
                    <p class="ellipsis">QQ：178304593</p>
                    <br>
                    <p><br></p>
                    <br>
                    <p>N设官网 ： <a target="_blank" href="http://www.uemo.net">http://www.ndesig.com</a></p>
                    <br>
                    <p><br></p>

                    <!--<p>微信公众账号：uemonet</p>-->

                    <p><br></p>

                    <p><br></p><br></td>
            </tr>
            </tbody>
        </table>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
    </div>
</div>



    {% widget "home:widget/home_footer/home_footer.tpl"%}
    {% widget "home:widget/sider_tools/sider_tools.tpl"%}
{% require "home:page/contact.tpl" %}{% endblock %}
