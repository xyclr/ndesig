{% extends 'home:page/layout.tpl' %}

{% block content %}
    {% require "home:static/css/inner.less" %}
    {% widget "home:widget/header/header.tpl"%}


<div style="background: #eee;margin-top: 80px;" >
    <div class="inner-content">
        <div class="post-detail">
            <div class="mt">
                <h3>{{post.title}}</h3>
                <strong>{{post.tags}}</strong>
            </div>
            <div class="mc">
                {{post.post}}
            </div>

        </div>
    </div>
</div>




    {% widget "home:widget/home_footer/home_footer.tpl"%}
    {% widget "home:widget/sider_tools/sider_tools.tpl"%}
{% require "home:page/caseDetail.tpl" %}{% endblock %}
