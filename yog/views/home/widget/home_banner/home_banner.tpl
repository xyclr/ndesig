<div class="banner">
    <ul>
        {% for post in postsBanner -%}
        <li style="background-image: url('{{post.thumb}}');">
            <div class="inner"></div>
        </li>
        {%- endfor %}

    </ul>
</div>
{% widget "home:widget/ui-swiper/swiper.tpl"%}
{% script %}
    $(function(){
$('.banner').unslider({
fluid: true,
dots: true
});
    })
{% endscript %}