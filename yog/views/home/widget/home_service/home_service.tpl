<div class="module service-bg">
        <div class="bgmask"></div>
        <div class="service module-box">
            <div class="clearfix module-tit">
                <div class="fadeInLeft">
                    <h2>服务介绍</h2>
                    <p >SERVICE</p>
                </div>
            </div>
            <div class="module-bot">
                <ul class="service-list">
                    <li>
                        <div class="service-step">
                            <i class="i-img iconfont icon-computer"></i>
                            <h4 class="service-title">Web应用产品解决方案</h4>
                            <p class="service-step-text">iOS/Android APP交互设计 视觉设计 功能定制开发 微信公众平台，基于HTML5的手机站</p>
                        </div>
                    </li>
                    <li>

                        <div class="service-step">
                            <i class="i-img iconfont icon-moblie"></i>
                            <h4 class="service-title">移动应用产品解决方案</h4>
                            <p class="service-step-text">iOS/Android APP交互设计媒体的核心价值在于其内容.</p>
                        </div>
                    </li>
                    <li>
                        <div class="service-step">
                            <i class="i-img iconfont icon-internet"></i>
                            <h4 class="service-title">网站及网络产品解决方案</h4>
                            <p class="service-step-text">网站及网络产品解决方案媒体的核心价值在于其内容</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
{% require "./home-service.less" %}