<ul class="news-ul clearfix">
    {% for post in postsNew -%}
    <li class="news_li">
        <div>
            <a href="/new/p/{{ post._id.toString() }}" class="news_bot_li_top">
                <span>{{post.title}}</span>
                <i>{{post.abstract}}</i>
            </a>
            <a href="/new/p/{{ post._id.toString() }}" class="news_bot_li_bot">
                <span>{{post.time.day}}</span>
            </a>
        </div>
        <div class="news_bot_bigli">
            <a href="/new/p/{{ post._id.toString() }}" class="news_bot_li_top news_bot_li_bigtop">
                <span>{{post.title}}</span>
                <i>{{post.abstract}}</i>

            </a>
            <a href="/new/p/{{ post._id.toString() }}" class="news_bot_li_bot news_bot_li_bigbot">
                <span>{{post.time.day}}</span>
                <strong href="/new/p/{{ post._id.toString() }}" class="show_list_but news_bot_but"><i></i></strong>
            </a>
        </div>
    </li>
    {% endfor %}
</ul>