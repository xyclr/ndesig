<div id="header">
    <div class="content">
        <a href="/" id="logo">
            <img src="/static/home/widget/header/i/logo_c8afb7c.png" height="40">

        </a>
        <ul id="nav">
            <li class="navitem" name="home">
                <a href="/" target="_self">首页</a>
            </li>
            <li class="navitem" name="case">
                <a href="/case" target="_self">案例</a>
            </li>
            <li class="navitem" name="new">
                <a href="/new" target="_self">新闻</a>
            </li>
            <!--<li class="navitem" name="lab"><a href="/case" target="_self">实验室</a></li>-->
            <!--<li class="navitem" name="blog"><a href="#" target="_self">博客</a></li>-->
            <li class="navitem" name="contact"><a href="/contact" target="_self">联系我们</a></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
{% require "./header.less" %}