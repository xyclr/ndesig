{% extends 'home:page/layout.tpl' %}
{% block content %}
<style>
    body {
        background-color: #f3f3f4;
    }
    * {box-sizing: content-box;}
    .reg-box {
        max-width: 300px;
        z-index: 100;
        margin: 0 auto;
        padding-top: 40px;
        width: 400px;
        text-align: center;
        font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    .logo-name {
        color: #e6e6e6;
        font-weight: 800;
        letter-spacing: -10px;
        margin-bottom: 0;
        font-size: 170px;
    }
    p {
        margin: 0 0 10px 0;
    }
</style>
<div class="reg-box">
    <div>
        <h1 class="logo-name">N+</h1>
        <h3 class="mb10">Register to Ndesig</h3>
        <p>Create account to see it in action.</p>
        <form class="m-t" role="form" method="post">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Name" required="" id="name" name="name">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" required="" id="email" name="email">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" required="" id="password" name="password">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Repeat Password" required="" id="password-repeat" name="password-repeat">
            </div>
            <div class="form-group">
                <div class="checkbox i-checks"><label class=""> <div class="icheckbox_square-green" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div><i></i> Agree the terms and policy </label></div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">Register</button>

            <p class="mt10"><small>Already have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="login">Login</a>
        </form>
    </div>
</div>
{% endblock %}

