{% extends 'home:page/layout.tpl' %}
{% block content %}
<style>
    body {
        background-color: #f3f3f4;
    }
    * {box-sizing: content-box;}
    .login-box {
        max-width: 400px;
        z-index: 100;
        margin: 0 auto;
        padding-top: 40px;
        width: 300px;
        text-align: center;
        font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    .logo-name {
        color: #e6e6e6;
        font-weight: 800;
        letter-spacing: -10px;
        margin-bottom: 0;
        font-size: 170px;
    }
    p {
        margin: 0 0 10px 0;
    }
</style>
<div class="login-box">
    <div>
        <h1 class="logo-name">N+</h1>
        <h3 class="mb10">Welcome to N+</h3>
        <form class="m-t" role="form" method="post">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username" required="" id="name" name="name">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" required="" id="password" name="password">
            </div>
            <button type="submit" class="btn btn-primary btn-block ">Login</button>

            <p class="mt10"><small>Do not have an account?</small></p>
            <a class="btn btn-white btn-block" href="reg">Create an account</a>
        </form>
    </div>

</div>
{% endblock %}

