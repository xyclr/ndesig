{% extends 'home:page/layout.tpl' %}

{% block content %}
    {% require "home:static/css/inner.less" %}
    {% widget "home:widget/header/header.tpl"%}
<div class="banner">
    <div style=" background-color: #39383E; background-repeat: no-repeat; background-position: center; background-size: cover;width: 100%; height: 380px;background-image: url(http://resources.uemo.net/templates/upload/4/201508/1439974604475.jpg);">

    </div>
</div>

<div style="background: #eee">
    <div class="inner-content">
        <div class="post-detail">
            <div class="mt">
                <h3>{{post.title}}</h3>
                <strong>{{post.tags}}</strong>
            </div>
            <div class="mc">
                {{post.post}}
            </div>

        </div>
    </div>
</div>




    {% widget "home:widget/home_footer/home_footer.tpl"%}
    {% widget "home:widget/sider_tools/sider_tools.tpl"%}
{% endblock %}
