{% extends 'home:page/layout_user.tpl' %}

{% block content %}

<div class="wrapper">

    {% require "home:static/css/summernote.css" %}

    {% include 'home:page/user/nav.tpl' %}


    <div class="page-wrapper">
        <div class="row white-bg page-heading">
            <div class="col-lg-12">
                <h3>编辑文章</h3>
                <ol class="breadcrumb">
                    <li>
                        <strong>发布</strong>
                    </li>
                    <li class="active">
                        <strong>编辑文章</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row mt20">
            <div class="col-lg-12">
                <div class="ibox-title">
                    <h5>编辑文章</h5>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" id="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="title">标题</label>
                            <div class="col-sm-10"><input type="text" class="form-control" value="{{post.title}}" id="title" name="title" placeholder="文章标题"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">标签</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div id="tag" tags="{{post.tags}}">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox">网页设计
                                                </label>
                                            </div>
                                        </div>

                                        <input type="hidden" placeholder=".标签"  value="" class="form-control" name="tag">

                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-4 collapse"><input type="text" placeholder="新增tag" class="form-control" name="tagNew"></div>
                                        <a href="#" class="mt10 f-fl J-add-tag" >新增</a>
                                        <a href="#" class="mt10 f-fl ml10 collapse J-save-tag">保存</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">推荐位置</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4"><input type="text" value="{{post.posi}}" placeholder="1（首页banner）" class="form-control" name="posi"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">分类</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select name="cat" id="cat" class="form-control" curIndex="{{post.cat}}">
                                            <option value="0">请选择分类</option>
                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="col-md-4 collapse"><input type="text" placeholder="新分类名称" class="form-control" name="catNew"></div>
                                        <a href="#" class="mt10 f-fl J-add-cat" >新增</a>
                                        <a href="#" class="mt10 f-fl ml10 collapse J-save-cat">保存</a>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!--extra start-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="thumb">缩略图</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="input-group" data-type="file">
                                            <input type="text" class="form-control cur" value="{{post.thumb}}" id="thumb" name="thumb" placeholder="请输入或者选择图片地址">
                                            <span class="input-group-btn"> <a class="btn btn-default btn-upload" data-toggle="modal" data-target="#fileUpload">选择</a> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">摘要</label>
                            <div class="col-sm-10">
                                <textarea name="abstract" class="form-control">请输入...</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">正文</label>
                            <div class="col-sm-10">
                                <!-- <textarea name="post" rows="20" cols="100" class="form-control" placeholder="请输入..."></textarea>-->
                                <textarea name="post"  class="summernote">{{post.post}}</textarea>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2"> <button type="submit" class="btn btn-primary J-post-update">发表</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{% require "home:static/js/summernote.js" %}
{% script %}
    var User = require("home:client/static/js/page/user.js");

    $(function(){

        User.init();

    })
{% endscript %}
{% endblock %}
