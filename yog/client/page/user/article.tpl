{% extends 'home:page/layout_user.tpl' %}

{% block content %}

<div class="wrapper">

    {% include 'home:page/user/nav.tpl' %}

    <div class="page-wrapper">
        <div class="row white-bg page-heading">
            <div class="col-lg-12">
                <h3>文章</h3>
                <ol class="breadcrumb">
                    <li>
                        <strong>发布</strong>
                    </li>
                    <li class="active">
                        <strong>文章</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row mt20">
            <div class="col-lg-12">
                <div class="ibox-title">
                    <h5>文章</h5>
                </div>
                <div class="ibox-content">
                    <div class="pull-right">
                        <a class="btn btn-primary btn-xs" href="edit/{{ post._id.toString() }}"> <i class="fa fa-edit"></i> 编辑</a>
                        <a class="btn btn-warning btn-xs" href="remove/{{ post._id.toString() }}"> <i class="fa fa-del"></i> 删除</a>
                        <a class="btn btn-warning btn-xs" href="comment/{{ post._id.toString() }}"> <i class="fa fa-del"></i>留言</a>
                    </div>
                    <div class="f-cb"></div>
                    <div class="text-center article-title">
                        <h1>
                            {{post.title}}
                        </h1>
                        <style>
                            .info-table {border:1px solid #ddd;margin: 20px auto;}
                            .info-table td {border:1px solid #ddd;padding:5px 10px;}
                            .info-table td strong {width:100px;text-align: right;}
                        </style>
                        <table class="info-table">
                            <tbody>
                            <tr>
                                <td align="right"><strong>推荐位置</strong>： </td>
                                <td><span class="text-muted">{{post.posi}} </span> </td>
                            </tr>
                            <tr>
                                <td align="right"><strong>tags</strong>： </td>
                                <td><span class="text-muted">{{post.tags}} </span> </td>
                            </tr>
                            <tr>
                                <td align="right"><strong>分类</strong>： </td>
                                <td><span class="text-muted">{{post.cat}} </span> </td>
                            </tr>
                            <tr>
                                <td align="right"><strong>发布时间</strong>： </td>
                                <td><span class="text-muted">{{post.time.day}} </span> </td>
                            </tr>
                            <tr>
                                <td align="right"><strong>作者</strong>： </td>
                                <td><span class="text-muted">{{user.name}} </span> </td>
                            </tr>
                            <tr>
                                <td align="right"><strong>访问次数</strong>： </td>
                                <td><span class="text-muted">{{post.extra.pv}} </span> </td>
                            </tr>
                            <tr>
                                <td align="right"><strong>摘要</strong>： </td>
                                <td><span class="text-muted">{{post.abstract}} </span> </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        <p></p><p>{{post.post}}</p><p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{% require "home:static/js/summernote.js" %}
{% script %}
var User = require("home:client/static/js/page/user.js");

$(function(){

    User.init();

})
{% endscript %}
{% endblock %}
