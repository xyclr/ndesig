<div id="sider-tools">
    <a class="qq" href="tencent://message/?uin=178304593&Site=www.ndesig.com&Menu=yes" target="_blank"><i class="iconfont icon-qq"></i></a>
    <a class="sshare" href="javascript:;" style="display:none;"><i class="iconfont icon-share"></i></a>
    <a href="javascript:;" class="sweixin"><i class="iconfont icon-weixin"></i></a>
    <a href="javascript:;" class="gotop J-gotop" style="visibility:hidden;"><i class="iconfont icon-arrup"></i></a>
</div>
{% require "./sider-tools.less" %}
{% script %}
    $(function(){
       //gotop
       var $top = $(".J-gotop");
       if($top.length) {
               $top.click(function(){
                   $('html, body').animate({scrollTop : 0},400);
                   return false;
               });
           };
       	$(window).scroll(function(){
               var $win = $(window);
               if($win.scrollTop() > $win.height()) {
                   $top.css({"visibility":"visible"});
               } else {
                   $top.css({"visibility":"hidden"});
               }
           }).trigger("scroll");


    })
{% endscript %}
