<div id="banner" class="swiper-container">
    <div class="swiper-wrapper">
        {% for post in postsBanner -%}
            <div class="swiper-slide"><img src="{{post.thumb}}" alt="" title="{{post.title}}"/></div>
        {%- endfor %}
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div>
<style>
    .swiper-container {
        width: 100%;
        height: 100%;
    }

    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;

        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
</style>
{% widget "home:widget/ui-swiper/swiper.tpl"%}
{% script %}
    $(function(){
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 30,
            loop: true
        });
    })
{% endscript %}